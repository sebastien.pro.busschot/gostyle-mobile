import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mspr_mobile_app/utils/constants.dart';
import 'package:mspr_mobile_app/views/cart_view.dart';
import 'package:mspr_mobile_app/views/home_view.dart';
import 'package:mspr_mobile_app/views/login_view.dart';
import 'package:mspr_mobile_app/views/products_view.dart';
import 'package:mspr_mobile_app/views/coupons_view.dart';


import '../custom_containers.dart';
import '../utils/session_storage.dart';

class GoStyleScaffold extends StatelessWidget {

  final GlobalKey<ScaffoldState> _drawerscaffoldkey = new GlobalKey<ScaffoldState>();
  final Widget body;
  final String title;

  GoStyleScaffold({this.body, this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(this.title),
        leading: IconButton(
          onPressed: () {
            //on drawer menu pressed
            if(_drawerscaffoldkey.currentState.isDrawerOpen) {
              //if drawer is open, then close the drawer
              Navigator.pop(context);
            } else {
              _drawerscaffoldkey.currentState.openDrawer();
              //if drawer is closed then open the drawer.
            }
          },
          icon: Icon(Icons.menu),

        ), // Set menu icon at leading of AppBar
        actions: [
          if( this.title != HOME_TITLE )
            IconButton(icon: Icon(Icons.keyboard_return), onPressed: () => Navigator.pop(context))
        ],
      ),
      body: Scaffold(
        key:_drawerscaffoldkey, //set gobal key defined above
        body: body,

        drawer: FractionallySizedBox (
          // Width half the screen
          widthFactor: 0.5,
          child: Drawer(
              elevation: 0,
              child: Container(
                alignment: FractionalOffset.center,
                color: gsVibrantBlue,
                child: ListView(
                  // Important: Remove any padding from the ListView.
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        homePageRoute(HOME_TITLE, HOME_TITLE == this.title, MyHomePage.routeName, context),
                        if ( SessionStorage().isAuthenticated() )
                          homePageRoute(SALES_TITLE, SALES_TITLE == this.title, CouponsPage.routeName, context),
                        if ( SessionStorage().isAuthenticated() )
                          homePageRoute(OFFERS_TITLE, OFFERS_TITLE == this.title, CartPage.routeName, context),
                        if ( SessionStorage().isAuthenticated() )
                          homePageRoute(PRODUCTS_TITLE, PRODUCTS_TITLE == this.title, ProductsPage.routeName, context),                        
                        if ( !SessionStorage().isAuthenticated() )
                          homePageRoute("Se connecter", false, LoginScreen.routeName, context),
                      ],
                    )
                  ],
                ),
              )
          ),
        ),
      ),
    );
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mspr_mobile_app/custom_containers.dart';
import 'package:mspr_mobile_app/models/coupon.dart';
import 'package:mspr_mobile_app/models/coupon_cart.dart';
import 'package:mspr_mobile_app/services/coupons_cart_service.dart';
import 'package:mspr_mobile_app/utils/constants.dart';
import 'package:mspr_mobile_app/utils/session_storage.dart';

import 'package:mspr_mobile_app/views/coupon_details_view.dart';

class CouponArguments {
  final Coupon coupon;

  CouponArguments(this.coupon);
}

class CouponWidget extends StatefulWidget {
  final Coupon coupon;

  final List<CouponCart> couponsCarts;

  CouponWidget(coupon, couponCart, {Key key})
      : this.coupon = coupon,
        this.couponsCarts = couponCart,
        super(key: key);

  @override
  _CouponWidgetState createState() =>
      _CouponWidgetState(this.coupon, this.couponsCarts);
}

class CouponDetailArguments {
  Coupon coupon;
  List<CouponCart> couponCart;

  CouponDetailArguments(coupon, couponCart)
      : this.coupon = coupon,
        this.couponCart = couponCart;
}

class _CouponWidgetState extends State<CouponWidget> {
  final Coupon coupon;
  List<CouponCart> couponsCarts;
  final CouponCartService service = CouponCartService();

  _CouponWidgetState(coupon, couponCart)
      : this.coupon = coupon,
        this.couponsCarts = couponCart;

  bool isFavorite = false;

  Future<void> _toggleFav() async {
    couponsCarts = await service.getAllCouponsCart();
    setState(() {
      if (isFavorite) {
        service.deleteCouponCart(this
            .couponsCarts
            .where((element) => element.coupon.couponId == this.coupon.couponId)
            .first
            .couponCartId);
      } else {
        service.addCouponCart(
            SessionStorage().getUserId(), this.coupon.couponId);
      }
      isFavorite = !isFavorite;
    });
  }

  @override
  void initState() {
    for (CouponCart entry in couponsCarts) {
      if (entry.coupon.couponId == coupon.couponId) {
        isFavorite = true;
        break;
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final String formatted = "Éxpire le " + dateFormatter.format(coupon.expiryDate) + " à " + timeFormatter.format(coupon.expiryDate);
    return InkWell(
        onTap: () {
          Navigator.pushNamed(context, CouponDetail.routeName,
              arguments: CouponDetailArguments(this.coupon, this.couponsCarts));
        },
        child: Container(
          decoration: widgetDecoration(),
          padding: EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    widgetText(
                      this.coupon.coupon,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.grey[700]
                      ),
                    ),
                    widgetText(
                      this.coupon.percentageDiscount.toString() + " %",
                      style: TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.bold,
                        color: gsVibrantBlue,
                        shadows: [
                          Shadow(
                            color: Color.fromRGBO(0, 0, 0, 0.5),
                            blurRadius: 5,
                            offset: Offset(1, 2)
                          )
                        ]
                      )
                    ),
                    widgetText(
                      this.coupon.couponDescription,
                      style: TextStyle(
                        fontSize: 14
                      )
                    ),
                    widgetText(
                      formatted,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.grey[600]
                      )
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  IconButton(
                      icon: isFavorite ? favoriteIcon : nonFavoriteIcon,
                      onPressed: _toggleFav)
                ],
              ),
            ],
          ),
        ));
  }
}


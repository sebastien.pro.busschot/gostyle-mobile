import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mspr_mobile_app/models/product.dart';
import 'package:mspr_mobile_app/views/product_details_view.dart';
import 'package:mspr_mobile_app/utils/string_extension.dart';

import '../custom_containers.dart';

class ProductArguments {
  final Product product;

  ProductArguments(this.product);
}

class ProductWidget extends StatelessWidget
{
  final Product product;
  ProductWidget(Product product)
      : this.product = product;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () { Navigator.pushNamed(context, ProductDetail.routeName, arguments: ProductArguments(this.product)); },
      child: Container (
        decoration: widgetDecoration(),
        padding: EdgeInsets.all(10),
        //color: Colors.grey[300],
        child:
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  this.product.product.trim().firstLetterUpper(),
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.grey[700]
                  ),
                ),
                Expanded(
                  child: Image.network(product.getImgUrl()),
                ),
              ],
          ),
        ),
      )
    );
  }
}
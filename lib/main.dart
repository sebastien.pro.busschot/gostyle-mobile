import 'package:flutter/material.dart';
import 'package:mspr_mobile_app/utils/constants.dart';

import 'package:mspr_mobile_app/views/cart_view.dart';
import 'package:mspr_mobile_app/views/coupon_details_view.dart';
import 'package:mspr_mobile_app/views/home_view.dart';
import 'package:mspr_mobile_app/views/login_view.dart';
import 'package:mspr_mobile_app/views/product_details_view.dart';
import 'package:mspr_mobile_app/views/products_view.dart';
import 'package:mspr_mobile_app/views/coupons_view.dart';
import 'package:mspr_mobile_app/views/register_view.dart';
import 'package:mspr_mobile_app/views/unknown_view.dart';
import 'package:mspr_mobile_app/widgets/coupon_widget.dart';
import 'package:mspr_mobile_app/widgets/product_widget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: (settings) {
        //settings.arguments;
        switch  (settings.name) {
          case MyHomePage.routeName:
            return MaterialPageRoute(builder: (context) => MyHomePage());
          case CartPage.routeName:
            return MaterialPageRoute(builder: (context) => CartPage());
          case ProductsPage.routeName:
            return MaterialPageRoute(builder: (context) => ProductsPage());
          case CouponsPage.routeName:
            return MaterialPageRoute(builder: (context) => CouponsPage());
          case LoginScreen.routeName:
            return MaterialPageRoute(builder: (context) => LoginScreen());              
          case RegisterPage.routeName:
            final List<String> l = settings.arguments;
            return MaterialPageRoute(builder: (context) => RegisterPage(l));
          case ProductDetail.routeName:
            final ProductArguments args = settings.arguments;
            return MaterialPageRoute(builder: (context) => ProductDetail(
              args.product,
            ));
          case CouponDetail.routeName:
            final CouponDetailArguments args = settings.arguments;
            return MaterialPageRoute(builder: (context) => CouponDetail(
              args.coupon,
              args.couponCart,
            ));
          default:
            return MaterialPageRoute(builder: (context) => UnknownPage());
        }
      },
      title: 'GoStyle',
      theme: ThemeData(

        primarySwatch: gsVibrantBlue,

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}
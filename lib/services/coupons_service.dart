import 'dart:convert';

import 'package:http/http.dart';
import 'package:mspr_mobile_app/models/coupon.dart';
import 'package:mspr_mobile_app/utils/session_storage.dart';
import 'package:mspr_mobile_app/utils/config.dart';

class CouponService {
  final String url = API_URL_HTTP + "/coupons";
  final String token = SessionStorage().getAuthenticationToken();

  Future<List<Coupon>> getAllCoupons() async {
    Response res = await get(Uri.parse(url), headers: {
      'Authorization': 'Bearer $token'
    });
    if(res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);
      List<Coupon> coupons = body.map((dynamic item) => Coupon.fromJson(item)).toList();

      return coupons;
    }
    else {
      throw "Can't get coupons.";
    }
  }

  Future<Coupon> getCouponById(int id) async {
    Response res = await get(Uri.parse(url+"/$id"), headers: {
      'Authorization': 'Bearer $token'
    });

    if(res.statusCode == 200) {
      return Coupon.fromJson(jsonDecode(res.body));
    }
    else {
      throw "Can't get coupons.";
    }
  }
}

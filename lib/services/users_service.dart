import 'dart:convert';

import 'package:http/http.dart';
import 'package:mspr_mobile_app/models/user.dart';
import 'package:mspr_mobile_app/utils/session_storage.dart';
import 'package:mspr_mobile_app/utils/config.dart';

class UserService {
  final String url = API_URL_HTTP + "/users";
  final String token = SessionStorage().getAuthenticationToken();

  Future<void> addUser(String username, String password, String surname, String lastname, String address, String city, int zipCode) async {
    Map<String, dynamic> data = {
      'username': username,
      'password': password,
      'surname' : surname,
      'lastname' : lastname,
      'address' : address,
      'city' : {
        'city': city,
        'zipCode' : zipCode
      }
    };

    Response res = await post(
      Uri.parse( API_URL_HTTP + "/register"),
      headers: {
          "content-type":"application/json"
      },
      body: jsonEncode(data)
    );
    if(res.statusCode == 200) {
      print('Inscription : SUCCESS ! ');
    }
    else if(res.statusCode == 409) {
      print('Username already used');
    }
    else {
      print('Inscription : ERROR ! [' + res.statusCode.toString() + ']');
    }
  }

  Future<User> getUserByUsername(String username) async {
    Response res = await get(Uri.parse(url+"/username=$username"), headers: {
      'Authorization': 'Bearer $token'
    });
    if(res.statusCode == 200) {
      return User.fromJson(jsonDecode(res.body));
    }
    else {
      throw "Can't get user.[" + res.statusCode.toString() + ']';
    }
  }
}
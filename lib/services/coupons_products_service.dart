import 'dart:convert';

import 'package:http/http.dart';
import 'package:mspr_mobile_app/models/coupon_product.dart';
import 'package:mspr_mobile_app/utils/session_storage.dart';
import 'package:mspr_mobile_app/utils/config.dart';

class CouponProductService {
  final String url = API_URL_HTTP + "/couponsProducts";
  final String token = SessionStorage().getAuthenticationToken();

  Future<List<CouponProduct>> getCouponsProductByCouponId(int couponId) async {
    Response res = await get(Uri.parse(url + '/coupon=$couponId'), headers: {
      'Authorization': 'Bearer $token'
    });

    if(res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<CouponProduct> coupons = body.map((dynamic item) => CouponProduct.fromJson(item)).toList();

      return coupons;
    }
    else {
      throw "Can't get coupons." + res.statusCode.toString();
    }
  }

  Future<List<CouponProduct>> getCouponsProductByProductId(int productId) async {
    Response res = await get(Uri.parse(url + '/product=$productId'), headers: {
      'Authorization': 'Bearer $token'
    });

    if(res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<CouponProduct> coupons = body.map((dynamic item) => CouponProduct.fromJson(item)).toList();

      return coupons;
    }
    else {
      throw "Can't get coupons." + res.statusCode.toString();
    }
  }
}
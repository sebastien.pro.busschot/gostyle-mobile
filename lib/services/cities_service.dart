import 'dart:convert';

import 'package:http/http.dart';
import 'package:mspr_mobile_app/models/city.dart';
import 'package:mspr_mobile_app/utils/config.dart';
import 'package:mspr_mobile_app/utils/session_storage.dart';

class CitiesService {
  final String url = API_URL_HTTP + "/cities";
  final String token = SessionStorage().getAuthenticationToken();

  Future<List<City>> getAllCities() async {
    Response res = await get(Uri.parse(url), headers: {
      'Authorization': 'Bearer $token'
    });


    if(res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<City> cities = body.map((dynamic item) => City.fromJson(item)).toList();

      return cities;
    }
    else {
      throw "Can't get cities.";
    }
  }

  Future<City> getCityById(int id) async {
    Response res = await get(Uri.parse(url+"/$id"), headers: {
      'Authorization': 'Bearer $token'
    });

    if(res.statusCode == 200) {
      return City.fromJson(jsonDecode(res.body));
    }
    else {
      throw "Can't get city.";
    }
  }

}
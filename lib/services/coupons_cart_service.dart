import 'dart:convert';

import 'package:http/http.dart';
import 'package:mspr_mobile_app/models/coupon_cart.dart';
import 'package:mspr_mobile_app/utils/config.dart';
import 'package:mspr_mobile_app/utils/session_storage.dart';

class CouponCartService {
  final String url = API_URL_HTTP + "/couponsCart";
  final String token = SessionStorage().getAuthenticationToken();

  Future<List<CouponCart>> getAllCouponsCart() async {
    Response res = await get(Uri.parse(url), headers: {
      'Authorization': 'Bearer $token'
    });

    if(res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<CouponCart> coupons = body.map((dynamic item) => CouponCart.fromJson(item)).toList();

      return coupons;
    }
    else {
      throw "Can't get coupons.";
    }
  }

  Future<void> addCouponCart(int userId, int couponId) async {
    Map<String, dynamic> data = {
      'user' : {
        'id': userId
      },
      'coupon' : {
        'couponId': couponId
      },
    };

    Response res = await post(
        Uri.parse(url),
        headers: {
          "content-type":"application/json",
          'Authorization': 'Bearer $token'
        },
        body: jsonEncode(data)
    );

    if(res.statusCode == 200) {
      //print(res.body);
      //return CouponCart.fromJson(jsonDecode(res.body));
    }
    else {
      throw 'ERROR ! [' + res.statusCode.toString() + ']';
    }
  }

  Future<void> deleteCouponCart(int id) async {
    Response res = await delete(
        Uri.parse(url+'/$id'),
        headers: {
          'Authorization': 'Bearer $token'
        }
    );

    if(res.statusCode == 200) {
      print('SUCCESS ! ');
    }
    else {
      print('ERROR ! [' + res.statusCode.toString() + ']');
    }
  }
}
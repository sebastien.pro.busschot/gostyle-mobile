import 'dart:convert';

import 'package:http/http.dart';
import 'package:mspr_mobile_app/models/product.dart';
import 'package:mspr_mobile_app/utils/session_storage.dart';
import 'package:mspr_mobile_app/utils/config.dart';

class ProductService {
  final String url = API_URL_HTTP + "/products";
  final String token = SessionStorage().getAuthenticationToken();

  Future<List<Product>> getAllProducts() async {
    Response res = await get(Uri.parse(url), headers: {
      'Authorization': 'Bearer $token'
    });

    //print("RESPONSE ${res.statusCode} ; BODY = ${res.body}");

    if(res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);

      List<Product> products = body.map((dynamic item) => Product.fromJson(item)).toList();

      return products;
    }
    else {
      throw "Can't get products.";
    }
  }

  Future<Product> getProductById(int id) async {
    Response res = await get(Uri.parse(url+"/$id"), headers: {
      'Authorization': 'Bearer $token'
    });

    if(res.statusCode == 200) {
      return Product.fromJson(jsonDecode(res.body));
    }
    else {
      throw "Can't get products.";
    }
  }

}
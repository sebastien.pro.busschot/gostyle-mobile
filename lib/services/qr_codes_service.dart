import 'dart:convert';

import 'package:http/http.dart';
import 'package:mspr_mobile_app/models/qr_code.dart';
import 'package:mspr_mobile_app/utils/config.dart';
import 'package:mspr_mobile_app/utils/session_storage.dart';

class QRCodeService {
  final String url = API_URL_HTTP + "/qrcodes";
  final String token = SessionStorage().getAuthenticationToken();

  Future<QRCode> getQRCodeById(int id) async {
    Response res = await get(Uri.parse(url+"/$id"), headers: {
      'Authorization': 'Bearer $token'
    });

    if(res.statusCode == 200) {
      return QRCode.fromJson(jsonDecode(res.body));
    }
    else {
      throw "Can't get QR_Code.";
    }
  }
}

import 'package:mspr_mobile_app/models/user.dart';

class SessionStorage {

  SessionStorage._privateConstructor();

  static final SessionStorage _instance = SessionStorage._privateConstructor();

  factory SessionStorage() {
    return _instance;
  }

  bool _authenticated = false;
  String _authToken = "";
  User _user;



  bool isAuthenticated() {
    return _authenticated;
  }

  String getAuthenticationToken()
  {
    if (_authenticated) {
      return _authToken;
    } else {
      return "";
    }
  }

  int getUserId()
  {
    return _authenticated ? this._user.id : 0;
  }

  String getUserName()
  {
    return _authenticated ? this._user.surname : "";
  }

  void connect( String _auth ) {
    this._authenticated = true;
    this._authToken = _auth;
  }
  void setUser( User user ) {
    this._user = user;
  }

  void disconnect() {
    this._authenticated = false;
    this._authToken = "";
  }
}
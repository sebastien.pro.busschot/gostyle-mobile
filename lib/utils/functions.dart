import 'package:flutter/material.dart';

String twoDigitsHex(int a)
{
  if (a > 255 || a < 0)
    throw new Exception("createMaterialColor() colors must be between 0 and 255");

  String ret = "";
  if (a <= 15)
    ret += "0";

  ret += a.toRadixString(16);
  return ret;
}

MaterialColor createMaterialColor(int r, int g, int b)
{
  Map<int, Color> color =
  {
    50:Color.fromRGBO(r, g, b, .1),
    100:Color.fromRGBO(r, g, b, .2),
    200:Color.fromRGBO(r, g, b, .3),
    300:Color.fromRGBO(r, g, b, .4),
    400:Color.fromRGBO(r, g, b, .5),
    500:Color.fromRGBO(r, g, b, .6),
    600:Color.fromRGBO(r, g, b, .7),
    700:Color.fromRGBO(r, g, b, .8),
    800:Color.fromRGBO(r, g, b, .9),
    900:Color.fromRGBO(r, g, b, 1),
  };

  String hexColor = "FF" + twoDigitsHex(r) + twoDigitsHex(g) + twoDigitsHex(b);
  return MaterialColor(int.parse(hexColor, radix: 16), color);
}

Text titleText(String _text)
{
  return new Text(
    _text,
    style: TextStyle(
      color: Colors.white
    )
  );
}


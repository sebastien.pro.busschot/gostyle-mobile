import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mspr_mobile_app/utils/functions.dart';

final MaterialColor materialGsVibranblue = createMaterialColor(40, 103, 174); // Vibrant blue
final Color gsVibrantBlue = materialGsVibranblue;
final Color gsLightBlue = createMaterialColor(94, 135, 157); // Light blue
final Color gsCanary = createMaterialColor(250, 187, 3); // Canary
final Color gsLightRed = createMaterialColor(229, 80, 82); // Light red
final Color gsDarkRed = createMaterialColor(100, 52, 66); // Dark red

final DateFormat dateFormatter = DateFormat.yMd();
final DateFormat timeFormatter = DateFormat.Hm();

final Icon favoriteIcon = Icon(Icons.favorite, size: 50, color: gsVibrantBlue);
final Icon nonFavoriteIcon = Icon(Icons.favorite_border, size: 50, color: gsVibrantBlue);

const double NAV_TEXT_FONT_SIZE = 18;
const FontWeight NAV_TEXT_FONT_WEIGHT = FontWeight.w500;
const TextStyle NAV_TEXT_STYLE = TextStyle( color: Colors.white, fontSize: NAV_TEXT_FONT_SIZE, fontWeight: NAV_TEXT_FONT_WEIGHT );
const TextStyle NAV_TEXT_DISABLED_STYLE = TextStyle( color: Colors.grey,  fontSize: NAV_TEXT_FONT_SIZE, fontWeight: NAV_TEXT_FONT_WEIGHT );

const NAV_TEXT_MARGIN = EdgeInsets.only(top: 20);

const String HOME_TITLE = 'Accueil';
const String SALES_TITLE = 'Promotion';
const String OFFERS_TITLE = 'Mes offres';
const String LOGIN_TITLE = 'Se connecter';
const String PRODUCTS_TITLE = 'Produits';
const String REGISTER_TITLE = 'Inscription';
const String QRCODES_TITLE = 'Scan qrcodes';

const TEST_IMG_URL = 'https://www.w3.org/Graphics/PNG/text2.png';

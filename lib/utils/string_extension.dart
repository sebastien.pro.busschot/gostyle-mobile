extension StringExtension on String {
  String firstLetterUpper() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}
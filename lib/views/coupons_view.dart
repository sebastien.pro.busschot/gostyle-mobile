import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mspr_mobile_app/models/coupon_cart.dart';
import 'package:mspr_mobile_app/services/coupons_cart_service.dart';
import 'package:mspr_mobile_app/services/coupons_service.dart';
import 'package:mspr_mobile_app/utils/constants.dart';
import 'package:mspr_mobile_app/widgets/coupon_widget.dart';
import 'package:mspr_mobile_app/widgets/gostyle_scafold.dart';

import '../custom_containers.dart';

class CouponsPage extends StatefulWidget {

  static const String routeName = '/coupons';

  CouponsPage({Key key}) : super(key: key);

  @override
  _CouponsPageState createState() => _CouponsPageState();
}

class _CouponsPageState extends State<CouponsPage> {

  final CouponService couponService = CouponService();
  final CouponCartService cartService = CouponCartService();

  List<CouponCart> couponsCarts;

  @override
  Widget build(BuildContext context) {
    return GoStyleScaffold(
      title: SALES_TITLE,
        body: futureBuilderItemList(cartService.getAllCouponsCart, (list) {
          couponsCarts = list;
          return generateGenericItemGrid(couponService.getAllCoupons, (item) => CouponWidget(item, couponsCarts), axisCount: 1);
        }, continueIfEmpty: true)
    );
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:mspr_mobile_app/models/qr_code.dart';
import 'package:mspr_mobile_app/services/coupons_cart_service.dart';
import 'package:mspr_mobile_app/services/qr_codes_service.dart';
import 'package:mspr_mobile_app/utils/constants.dart';
import 'package:mspr_mobile_app/views/coupon_details_view.dart';
import 'package:mspr_mobile_app/widgets/coupon_widget.dart';
import 'package:mspr_mobile_app/widgets/gostyle_scafold.dart';

import '../utils/session_storage.dart';
import 'login_view.dart';

class MyHomePage extends StatefulWidget {

  static const String routeName = '/';

  MyHomePage({Key key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  QRCodeService qrCodeService = QRCodeService();
  CouponCartService couponCartService = CouponCartService();

  Future<void> scanQR() async {
  String barcodeScanRes;
  // Platform messages may fail, so we use a try/catch PlatformException.
  try {
    barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        "#ff6666", "Cancel", true, ScanMode.QR);
    print(barcodeScanRes);
  } on PlatformException {
    barcodeScanRes = 'Failed to get platform version.';
  }

  // If the widget was removed from the tree while the asynchronous platform
  // message was in flight, we want to discard the reply rather than calling
  // setState to update our non-existent appearance.
  if (!mounted) return;

  try {
      var qrcodeId = int.parse(barcodeScanRes.split("==")[1]);

    QRCode qrc = await qrCodeService.getQRCodeById(qrcodeId);
    

    // this.coupon = await couponService.getCouponById( qrc.coupon.couponId);
    print(qrc.coupon.couponId.toString()+qrc.coupon.coupon);
    final CouponDetailArguments args = CouponDetailArguments(qrc.coupon,await couponCartService.getAllCouponsCart());
    Navigator.pushNamed(context, CouponDetail.routeName,arguments:args);
  } catch (e) {
    print(e);
  }

  // this.coupon = await couponService.getCouponById( int.parse(barcodeScanRes.split("==")[1]));
  // setState(() {
  //   _scanBarcode = barcodeScanRes.split("==")[1];
  //   Navigator.popAndPushNamed(context, CouponDetail.routeName,arguments: this.coupon);
  // });
}

  @override
  Widget build(BuildContext context) {

    String text = SessionStorage().isAuthenticated() ? "Bonjour " + SessionStorage().getUserName()  + ",\n\n": "";
    return GoStyleScaffold(
      title: HOME_TITLE,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 150, left: 50, right: 50, bottom: 50),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "GoStyle",
                style: TextStyle(
                  fontSize: 50,
                  fontWeight: FontWeight.bold,
                  color: gsVibrantBlue,
                ),
              ),
              Row(
                  children : [
                    Spacer(),
                    Container(
                      child: Expanded(
                        flex: 15,
                        child: Text(
                          text + "Bienvenue sur la page d'accueil de l'application GoStyle.",
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                    Spacer(),
                  ]
              ),
              Row(
                children: [
                  Spacer(),
                  Expanded(
                    flex: 15,
                    child: ElevatedButton(
                      onPressed: () {
                        if ( !SessionStorage().isAuthenticated() )
                          Navigator.pushNamed(context, LoginScreen.routeName);
                        else
                        {
                          SessionStorage().disconnect();
                          Navigator.popAndPushNamed(context, MyHomePage.routeName);
                        }
                      },
                      child: SessionStorage().isAuthenticated() ? Text('SE DÉCONNECTER') : Text('SE CONNECTER'),
                    ),
                  ),
                  Spacer(),
                ]
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    // Three spacers
                    for (var i = 0; i < 3; i++) Spacer(),
                    Expanded(
                      child: Ink(
                        decoration: ShapeDecoration(
                          color: gsVibrantBlue,
                          shape: CircleBorder(),
                        ),
                        child: SessionStorage().isAuthenticated() ? IconButton(icon: Icon(Icons.qr_code_rounded, color: Colors.white), onPressed: (){
                          scanQR();
                        }):Container(),
                      ),
                    ),
                  ]
              ),
            ],
          ),
        ),
      ),
    );
  }
}
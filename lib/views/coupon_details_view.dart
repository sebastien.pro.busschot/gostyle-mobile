import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mspr_mobile_app/custom_containers.dart';
import 'package:mspr_mobile_app/models/coupon.dart';
import 'package:mspr_mobile_app/models/coupon_cart.dart';
import 'package:mspr_mobile_app/models/coupon_product.dart';
import 'package:mspr_mobile_app/services/coupons_cart_service.dart';
import 'package:mspr_mobile_app/services/coupons_products_service.dart';
import 'package:mspr_mobile_app/utils/constants.dart';
import 'package:mspr_mobile_app/utils/session_storage.dart';
import 'package:mspr_mobile_app/widgets/gostyle_scafold.dart';

import 'package:mspr_mobile_app/utils/string_extension.dart';
import 'package:mspr_mobile_app/widgets/product_widget.dart';


class CouponDetail extends StatefulWidget {

  static const routeName = '/coupon';

  final Coupon coupon;
  List<CouponCart> couponCart;
  CouponDetail(Coupon coupon, couponCart, {Key key}) : this.coupon = coupon, this.couponCart = couponCart, super(key: key);

  @override
  _CouponDetailState createState() => _CouponDetailState(this.coupon, this.couponCart);
}

class _CouponDetailState extends State<CouponDetail> {

  _CouponDetailState(coupon, couponCarts): this.coupon = coupon, this.couponCarts = couponCarts;
  final Coupon coupon;

  final CouponCartService service = CouponCartService();
  final CouponProductService couponProductservice = CouponProductService();

  List<CouponCart> couponCarts;
  bool isFavorite = false;

  Future<void> _toggleFav() async {
    couponCarts = await service.getAllCouponsCart();
    setState(() {
      if (isFavorite) {
        service.deleteCouponCart(this.couponCarts.where((element) => element.coupon.couponId == this.coupon.couponId).first.couponCartId );
      } else {
        service.addCouponCart(SessionStorage().getUserId(), this.coupon.couponId);
      }
      isFavorite = !isFavorite;
    });
  }

  @override
  void initState() {
    for (CouponCart entry in couponCarts) {
      if (entry.coupon.couponId == coupon.couponId) {
        isFavorite = true;
        break;
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    const double padding = 50;
    return GoStyleScaffold(
      title: this.coupon.coupon.trim().firstLetterUpper(),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.only(bottom: padding),
          child: Column(
            children:[
              Container(
                padding: EdgeInsets.only(top: 30, bottom: 10, right: 10, left: 10),
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      materialGsVibranblue[200],
                      Colors.grey[50]
                    ]
                  )
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      this.coupon.coupon.trim().firstLetterUpper(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.bold,
                        color: gsVibrantBlue,
                        shadows: [
                          Shadow(
                          color: Color.fromRGBO(0, 0, 0, 0.5),
                            blurRadius: 5,
                            offset: Offset(1, 2)
                          )
                        ]
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(
                        maxHeight: 180,
                        minHeight: 180,
                      ),
                      child: generateGenericItemGrid( () => couponProductservice.getCouponsProductByCouponId(coupon.couponId), _getProductWidgetFromCouponProduct, shrink: true),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 25, right: 25, top: 10, bottom: 10),
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "-" + this.coupon.percentageDiscount.toString() + "%",
                              style: TextStyle(
                                color: gsVibrantBlue,
                                fontSize: 44,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            IconButton(
                                icon: isFavorite ? favoriteIcon : nonFavoriteIcon,
                                onPressed: _toggleFav
                            ),
                          ]
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(left: padding, right: padding),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Divider(),
                      Text(
                        this.coupon.couponDescription,
                        style: TextStyle(

                        ),
                        textAlign: TextAlign.justify,
                      ),
                      Divider(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Text(
                              "Applicable sous conditions :",
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.grey[700]
                              ),
                            ),
                          ),
                          Text(this.coupon.conditions),
                        ],
                      ),
                      Divider(),
                      Text(
                        "Coupons émis le " + dateFormatter.format(this.coupon.issueDate) + " à " + timeFormatter.format(this.coupon.issueDate),
                        style: TextStyle(color: Colors.grey[700]),
                      ),
                      Text(
                        "Le coupon expire le " + dateFormatter.format(this.coupon.expiryDate) + " à " + timeFormatter.format(this.coupon.expiryDate),
                        style: TextStyle(color: Colors.grey[700]),
                      ),
                      Row(),
                    ]
                  ),
                ),
              )
            ]
          ),
        )
      )
    );
  }

  Widget _getProductWidgetFromCouponProduct( dynamic item ) {
    CouponProduct couponProduct = item;
    return ProductWidget(couponProduct.product);
  }

}
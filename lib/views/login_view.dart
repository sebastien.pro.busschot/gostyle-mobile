import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mspr_mobile_app/models/user.dart';
import 'package:mspr_mobile_app/services/users_service.dart';

import 'package:mspr_mobile_app/utils/constants.dart';
import 'package:mspr_mobile_app/views/register_view.dart';
import 'package:mspr_mobile_app/widgets/gostyle_scafold.dart';

import '../utils/session_storage.dart';
import 'package:mspr_mobile_app/utils/config.dart';

import 'package:http/http.dart' as http;

import 'package:flutter_login/flutter_login.dart';

import 'home_view.dart';

class LoginScreen extends StatelessWidget {
  static const String routeName = '/login';
  LoginData registerData;
  bool isLogin = true;

  UserService userService;

  Future<String> connect(LoginData loginData) async {
    this.isLogin = true;
    http.Response response = await http.post(
      Uri.http(API_URL, "/auth/token"),
      body: {
        "username": loginData.name,
        "password": loginData.password,
        "grant_type": "password",
      },
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": 'Basic Zmx1dHRlcjo=',
      },
    );
    Map<String, dynamic> body = jsonDecode(response.body);
    switch (response.statusCode) {
      case 200:
        SessionStorage().connect(body["access_token"],);
        userService = UserService();
        User user = await userService.getUserByUsername(loginData.name);
        SessionStorage().setUser(user);
        return null;
      case 400:
        if (body.containsKey("error")) {
          if (body["error"] == "invalid_grant") {
            return "Nom d'utilisateur ou mot de passe invalide";
          }
        }
        return "Erreur 400";
      default:
        return "Erreur" + response.statusCode.toString();
    }
  }

  Future<String> register(LoginData registerData) async {
    this.isLogin = false;
    this.registerData = registerData;
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return GoStyleScaffold(
        title: LOGIN_TITLE,
        body: FlutterLogin(
          onRecoverPassword: (bla) => null,
          onSignup: register,
          onLogin: connect,
          onSubmitAnimationCompleted: () {
            if (this.isLogin) {
              Navigator.popAndPushNamed(context, MyHomePage.routeName);
            }
            else{
              List<String> listArg = [registerData.name,registerData.password];
              Navigator.popAndPushNamed(context, RegisterPage.routeName, arguments: listArg);
            }
          },

          messages: LoginMessages(
            loginButton: 'SE CONNECTER',
            signupButton: 'S\'INSCRIRE',
            forgotPasswordButton: 'Mot de passe oublié?',
            passwordHint: 'Mot de passe',
            confirmPasswordHint: 'Confirmer',
            confirmPasswordError: 'Les mots de passe ne correspondent pas',
          ),
          title: 'GoStyle',
          // TODO: Implement validators
          emailValidator: (a) => null,
          passwordValidator: (a) => null,
          theme: LoginTheme(
              pageColorLight: Colors.white,
              pageColorDark: Colors.white,
              primaryColor: gsVibrantBlue,
              titleStyle: TextStyle(color: gsVibrantBlue)),
        ));
  }
}

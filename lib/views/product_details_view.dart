import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mspr_mobile_app/models/product.dart';
import 'package:mspr_mobile_app/utils/constants.dart';
import 'package:mspr_mobile_app/widgets/gostyle_scafold.dart';

import 'package:mspr_mobile_app/utils/string_extension.dart';


class ProductDetail extends StatefulWidget {

  static const routeName = '/product';

  final Product product;
  ProductDetail(Product product, {Key key}) : this.product = product, super(key: key);

  @override
  _ProductDetailState createState() => _ProductDetailState(this.product);
}

class _ProductDetailState extends State<ProductDetail> {

  _ProductDetailState(this.product);
  final Product product;

  @override
  Widget build(BuildContext context) {
    const textPadding = EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10);
    const priceTextStyle = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 17,
    );
    const qteTextStyle = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 30,
    );

    return GoStyleScaffold(
        title: this.product.product.trim().firstLetterUpper(),
        body: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(top:10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      this.product.product.trim().firstLetterUpper(),
                      style: TextStyle(
                        fontSize: 36,
                        color: gsVibrantBlue,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Image.network(this.product.getImgUrl()),
                  ],
                ),
              ),
              Divider(),
              Container(
                padding: textPadding,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        this.product.description.toString(),
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          color: Colors.grey[700],
                        ),
                      ),
                    )
                  ]
                ),
              ),
              Divider(),
              Container(
                padding: textPadding,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                            "Quantité disponible",
                          style: priceTextStyle,
                        ),
                        Text(
                          this.product.quantity.toString(),
                          style: qteTextStyle,
                        ),
                      ]
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Prix unitaire",
                          style: priceTextStyle,
                        ),
                        Text(
                          this.product.unitPrice.toString() + "€",
                          style: qteTextStyle,
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          )
        )
    );
  }

}
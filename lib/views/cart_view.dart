import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mspr_mobile_app/models/coupon.dart';
import 'package:mspr_mobile_app/models/coupon_cart.dart';
import 'package:mspr_mobile_app/services/coupons_cart_service.dart';
import 'package:mspr_mobile_app/services/coupons_service.dart';
import 'package:mspr_mobile_app/utils/constants.dart';
import 'package:mspr_mobile_app/widgets/coupon_widget.dart';
import 'package:mspr_mobile_app/widgets/gostyle_scafold.dart';

import '../custom_containers.dart';

class CartPage extends StatefulWidget {
  static const String routeName = '/offers';

  CartPage({Key key}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {

  final CouponCartService cartService = CouponCartService();
  final CouponService couponService = CouponService();

  List<CouponCart> couponsCarts;

  @override
  Widget build(BuildContext context) {
    return GoStyleScaffold(
        title: OFFERS_TITLE,
        body: futureBuilderItemList(cartService.getAllCouponsCart, (list) {
          couponsCarts = list;
          return generateGenericItemGrid(cartService.getAllCouponsCart,
                  (item) {
                CouponCart tmp = item;
                Coupon coupon = tmp.coupon;
                return CouponWidget(coupon, couponsCarts);
              },
              axisCount: 1);
          }
        )
    );
  }
}

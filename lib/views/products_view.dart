import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mspr_mobile_app/services/products_service.dart';
import 'package:mspr_mobile_app/utils/constants.dart';
import 'package:mspr_mobile_app/widgets/gostyle_scafold.dart';
import 'package:mspr_mobile_app/widgets/product_widget.dart';

import '../custom_containers.dart';

class ProductsPage extends StatefulWidget {
  static const String routeName = '/products';

  ProductsPage({Key key}) : super(key: key);

  @override
  _ProductsPageState createState() => _ProductsPageState();
}

class _ProductsPageState extends State<ProductsPage> {

  final ProductService service = ProductService();

  @override
  Widget build(BuildContext context) {
    //print(service.getAllProducts());
    return GoStyleScaffold(
      title: PRODUCTS_TITLE,
      body: generateGenericItemGrid(service.getAllProducts, (item) => ProductWidget(item))
    );
  }
}

import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/material.dart';
import 'package:mspr_mobile_app/models/city.dart';
import 'package:mspr_mobile_app/services/cities_service.dart';
import 'package:mspr_mobile_app/services/users_service.dart';
import 'package:mspr_mobile_app/utils/constants.dart';
import 'package:mspr_mobile_app/views/login_view.dart';
import 'package:mspr_mobile_app/widgets/gostyle_scafold.dart';

class RegisterPage extends StatefulWidget {
  static const String routeName = '/register';

  final List<String> list;
  RegisterPage(list, {Key key})
      : this.list = list,
        super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState(this.list);
}

class _RegisterPageState extends State<RegisterPage> {

  final List<String> list;
  _RegisterPageState(this.list);

  final CitiesService service = CitiesService();
  final firstnameController = TextEditingController();
  final lastnameController = TextEditingController();
  final addressController = TextEditingController();
  final cityController = TextEditingController();

  List<City> citiesVObject2;
  final serviceUser = UserService();

  @override
  Widget build(BuildContext context) {
    return GoStyleScaffold(
        title: REGISTER_TITLE,
        body: SingleChildScrollView(
            child: Container(
          // decoration: BoxDecoration(
          //   gradient: LinearGradient(
          //       begin: Alignment.topCenter,
          //       end: Alignment.bottomCenter,
          //       colors: [materialGsVibranblue[50], Colors.grey[50]]),
          // ),
          child: Padding(
            padding: EdgeInsets.all(30),
            child: Column(children: [
              // Text(list[0]),
              // Text(list[1]),
              TextFormField(
                controller: firstnameController,
                decoration: const InputDecoration(
                  hintText: 'Prenom',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
              Container(
                height: 20,
              ),
              TextFormField(
                controller: lastnameController,
                decoration: const InputDecoration(
                  hintText: 'Nom',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
              Container(
                height: 20,
              ),
              TextFormField(
                controller: addressController,
                decoration: const InputDecoration(
                  hintText: 'Adresse',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
              Container(
                height: 20,
              ),
              FutureBuilder<List<City>>(
                future: service
                    .getAllCities(), // a previously-obtained Future<String> or null
                builder:
                    (BuildContext context, AsyncSnapshot<List<City>> snapshot) {
                  Widget child;

                  if (snapshot.hasData) {
                    List<String> cities = [];
                    List<City> citiesVObject = snapshot.data;
                    this.citiesVObject2 = citiesVObject;
                    for (var city in citiesVObject) {
                      cities.add(city.city + "-" + city.zipCode.toString());
                    }
                    String cityId;
                    child = DropDownField(

                      controller: cityController,
                      onValueChanged: (value) {
                        cityId = value;
                      },
                      value: cityId,
                      required: false,
                      hintText: "Selectionnez une ville",
                      labelText: 'Ville',
                      items: cities,
                    );
                  } else if (snapshot.hasError) {
                    child = Column(
                      children: [
                        const Icon(
                          Icons.error_outline,
                          color: Colors.red,
                          size: 60,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Text('Error: ${snapshot.error}'),
                        )
                      ],
                    );
                  } else {
                    child = Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            child: CircularProgressIndicator(),
                            width: 60,
                            height: 60,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 16),
                            child: Text('Awaiting result...'),
                          )
                        ]);
                  }
                  return Center(
                    child: child,
                  );
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                  onPressed: () {
                    // print(firstnameController.text);
                    // print(lastnameController.text);
                    // print(addressController.text);
                    // print(cityController.text);
                    String cityZipString = cityController.text;
                    int cityZip = int.parse(cityZipString.split("-")[1]);
                    City cityFinal;
                    for (var city in citiesVObject2) {
                      cityFinal = city.zipCode == cityZip ? city : null;
                    }
                    serviceUser.addUser(
                        list[0],
                        list[1],
                        firstnameController.text,
                        lastnameController.text,
                        addressController.text,
                        cityFinal.city,
                        cityFinal.zipCode);
                    // Validate will return true if the form is valid, or false if
                    // the form is invalid.
                    Navigator.popAndPushNamed(context, LoginScreen.routeName);
                  },
                  child: const Text('Valider'),
                ),
              ),
            ]),
          ),
        )));
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mspr_mobile_app/utils/constants.dart';
import 'package:mspr_mobile_app/utils/string_extension.dart';

const TextStyle EMPTY_STYLE = TextStyle();

Text widgetText( String str, {TextStyle style = EMPTY_STYLE})
{
  return Text(
    str.trim().firstLetterUpper(),
    style: style,
    overflow: TextOverflow.ellipsis,
  );
}

BoxDecoration widgetDecoration() {
  return BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            materialGsVibranblue[50],
            materialGsVibranblue[200],
          ]
      ),
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.1),
          spreadRadius: 5,
          blurRadius: 5,
          offset: Offset(-2, -2),
        )
      ]
  );
}

InkWell homePageRoute( String _text, bool disabled, String _route, BuildContext _context )
{
  return InkWell(
    child: Container(
      margin: NAV_TEXT_MARGIN,
      child: Text(
          _text,
          style: disabled ? NAV_TEXT_DISABLED_STYLE : NAV_TEXT_STYLE
      ),
    ),
    onTap: () {
      if (!disabled)
        Navigator.popAndPushNamed(_context, _route);
    },
  );
}

Widget futureBuilderItemList( Future<List<Object>> getItems(), Widget getWidget(list), {bool continueIfEmpty = false} )
{
  return FutureBuilder(
      future: getItems(),
      builder: (BuildContext context, AsyncSnapshot<List<Object>> snapshot) {
        Widget child; // Widget to be returned
        if (snapshot.hasData) {
          // If data is ready, cast it to Object List
          List<Object> list = snapshot.data;
          if (!continueIfEmpty) {
            if (list.length != 0)
              child = getWidget(list);
          }
          else child = getWidget(list);

        } else if (snapshot.hasError) {
          child = Column(
            children: [
              const Icon(
                Icons.error_outline,
                color: Colors.red,
                size: 60,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Text('Error: ${snapshot.error}'),
              )
            ],
          );
        } else {
          child = Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  child: CircularProgressIndicator(),
                  width: 60,
                  height: 60,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: Text('Awaiting result...'),
                )
              ]);
        }
        return Center(
          child: child,
        );
      },
  );
}

Widget generateGenericItemGrid( Future<List<Object>> getItems(), Widget createWidgetFromItem(item),
    {int axisCount = 2, bool shrink = false, bool continueIfEmpty = false}) {
  /*
  * This function generates a Grid view of generic items
  *      getItems is a service functions that must returns a Future List of wanted Items
  *      createWidgetFromItem is a function that takes an Item as argument, create and returns the widget
  *         for the given Item.
  *
  * */
  return futureBuilderItemList( getItems,
    (list) => GridView.count(
            scrollDirection: Axis.vertical,
            shrinkWrap: shrink,
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
            // Create a grid with 2 columns. If you change the scrollDirection to
            // horizontal, this produces 2 rows.
            childAspectRatio: 2/axisCount,
            crossAxisCount: axisCount,
            // Generate a widget for each element in the List
            children: List.generate(
                list.length, (index) => createWidgetFromItem(list[index])
            ),
          )
    ,continueIfEmpty: continueIfEmpty);
}

import 'package:flutter/cupertino.dart';
import 'package:mspr_mobile_app/models/coupon.dart';
import 'package:mspr_mobile_app/models/product.dart';

class CouponProduct {
  final int couponProductId;
  final Product product;
  final Coupon coupon;

  CouponProduct({
    @required this.couponProductId,
    @required this.product,
    @required this.coupon,
  });

  factory CouponProduct.fromJson(Map<String, dynamic> json) {
    return CouponProduct(
        couponProductId: json['couponProductId'] as int,
        product: new Product.fromJson(json['product']),
        coupon: new Coupon.fromJson(json['coupon'])
    );
  }

}
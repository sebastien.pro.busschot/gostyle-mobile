import 'package:flutter/cupertino.dart';

class Coupon {
  final int couponId;
  final String coupon;
  final DateTime issueDate;
  final DateTime expiryDate;
  final double percentageDiscount;
  final String couponDescription;
  final String conditions;
  final String barCode;
  final bool deleted;

  Coupon({
    @required this.couponId,
    @required this.coupon,
    @required this.issueDate,
    @required this.expiryDate,
    @required this.percentageDiscount,
    @required this.couponDescription,
    @required this.conditions,
    @required this.barCode,
    @required this.deleted
  });

  factory Coupon.fromJson(Map<String, dynamic> json) {
    return Coupon(
        couponId: json['couponId'] as int,
        coupon: json['coupon'] as String,
        issueDate: DateTime.parse(json['issueDate']),
        expiryDate: DateTime.parse(json['expiryDate']),
        percentageDiscount: json['percentageDiscount'] as double,
        couponDescription: json['couponDescription'] as String,
        conditions: json['conditions'] as String,
        barCode: json['barCode'] as String,
        deleted: json['deleted'] as bool
    );
  }

}
import 'package:flutter/cupertino.dart';

class Role {
  final int roleId;
  final String role;

  Role({
    @required this.roleId,
    @required this.role,
  });

  factory Role.fromJson(Map<String, dynamic> json) {
    return Role(
        roleId: json['roleId'] as int,
        role: json['role'] as String,
    );
  }
}
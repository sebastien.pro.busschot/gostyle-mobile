import 'package:flutter/cupertino.dart';
import 'package:mspr_mobile_app/models/coupon.dart';
import 'package:mspr_mobile_app/models/user.dart';

class CouponCart {
  final int couponCartId;
  final User user;
  final Coupon coupon;

  CouponCart({
    @required this.couponCartId,
    @required this.user,
    @required this.coupon,
  });

  factory CouponCart.fromJson(Map<String, dynamic> json) {
    return CouponCart(
        couponCartId: json['couponCartId'] as int,
        user: new User.fromJson(json['user']),
        coupon: new Coupon.fromJson(json['coupon']),
    );
  }
}
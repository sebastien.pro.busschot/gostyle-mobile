import 'package:flutter/cupertino.dart';
import 'package:mspr_mobile_app/models/city.dart';
import 'package:mspr_mobile_app/models/role.dart';

class User {
  final int id;
  final String surname;
  final String lastname;
  final String password;
  final String address;
  final bool deleted;
  final City city;
  final Role role;

  User({
    @required this.id,
    @required this.surname,
    @required this.lastname,
    @required this.password,
    @required this.address,
    @required this.deleted,
    @required this.city,
    @required this.role,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        id: json['id'] as int,
        surname: json['surname'] as String,
        lastname: json['lastname'] as String,
        password: json['password'] as String,
        address: json['address'] as String,
        deleted: json['deleted'] as bool,
        city: City.fromJson(json['city']),
        role: Role.fromJson(json['role']),
    );
  }
}
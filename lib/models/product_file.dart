import 'package:flutter/cupertino.dart';
import 'package:mspr_mobile_app/models/product.dart';

class ProductFile {
  final int fileId;
  final String fileName;
  final Product product;

  ProductFile({
    @required this.fileId,
    @required this.fileName,
    @required this.product,
  });

  factory ProductFile.fromJson(Map<String, dynamic> json) {
    return ProductFile(
        fileId: json['fileId'] as int,
        fileName: json['fileName'] as String,
        product: json['product'] as Product
    );
  }
}

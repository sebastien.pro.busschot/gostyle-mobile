import 'package:flutter/cupertino.dart';

class City {
  final int cityId;
  final int zipCode;
  final String city;

  City({
    @required this.cityId,
    @required this.zipCode,
    @required this.city,
  });

  factory City.fromJson(Map<String, dynamic> json) {
    return City(
        cityId: json['cityId'] as int,
        zipCode: json['zipCode'] as int,
        city: json['city'] as String
    );
  }
}
import 'package:flutter/cupertino.dart';
import 'package:mspr_mobile_app/models/coupon.dart';

class QRCode {
  final int qrCodeId;
  final String content;
  final Coupon coupon;

  QRCode({
    @required this.qrCodeId,
    @required this.content,
    @required this.coupon,
  });

  factory QRCode.fromJson(Map<String, dynamic> json) {
    return QRCode(
        qrCodeId: json['qrCodeId'] as int,
        content: json['content'] as String,
        coupon: new Coupon.fromJson(json['coupon'])
    );
  }
}
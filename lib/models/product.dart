import 'package:flutter/cupertino.dart';
import 'package:mspr_mobile_app/models/product_file.dart';
import 'package:mspr_mobile_app/utils/config.dart';
import 'package:mspr_mobile_app/utils/constants.dart';

class Product {
  final int productId;
  final String product;
  final String description;
  final int quantity;
  final int unitPrice;
  final String siteUrl;
  final bool deleted;
  final ProductFile file;


  Product({
    @required this.productId,
    @required this.product,
    @required this.description,
    @required this.quantity,
    @required this.unitPrice,
    @required this.siteUrl,
    @required this.deleted,
    @required this.file,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
        productId: json['productId'] as int,
        product: json['product'] as String,
        description: json['description'] as String,
        quantity: json['quantity'] as int,
        unitPrice: json['unitPrice'] as int,
        siteUrl: json['siteUrl'] as String,
        deleted: json['deleted'] as bool,
        file: new ProductFile.fromJson(json['file'])
    );
  }

  String getImgUrl()
  {
    if(this.file == null || this.file.fileName == null){
      return TEST_IMG_URL;
    }

    return API_URL_HTTP + "/files/show/" + this.file.fileName;
  }
}
